call plug#begin('~/.local/share/nvim/plugged')

Plug 'ciaranm/securemodelines'
" Plug 'embear/vim-localvimrc'
Plug 'lervag/file-line'



Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update
Plug 'Shougo/vimproc.vim', {'do' : 'make'}

Plug 'mechatroner/rainbow_csv'
Plug 'derekwyatt/vim-fswitch'
Plug 'tomtom/tcomment_vim'
Plug 'ms-jpq/chadtree', {'branch': 'chad', 'do': 'python3 -m chadtree deps'} 
Plug 'nathanaelkane/vim-indent-guides'
Plug 'preservim/tagbar'
Plug 'lambdalisue/suda.vim'


Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'


Plug 'honza/vim-snippets'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'fannheyward/telescope-coc.nvim'


Plug 'tpope/vim-fugitive'
Plug 'mhinz/vim-signify'

Plug 'summivox/vim-nfo'
Plug 'leafgarland/typescript-vim'
Plug 'drewtempelmeyer/palenight.vim'


Plug 'chriskempson/tomorrow-theme'
Plug 'sheerun/vim-polyglot'
Plug 'https://gitlab.com/yorickpeterse/happy_hacking.vim.git'

call plug#end()
