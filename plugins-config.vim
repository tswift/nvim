
" localvimrc
let g:localvimrc_persistent = 1
let g:localvimrc_sandbox = 0


" nerdtree

nmap <leader>N :CHADopen<CR>

"vim-indent-guides
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_exclude_filetypes = ['help', 'nerdtree']

" nmap <leader>a :FSHere<CR>
nmap <leader>f :Telescope find_files<CR>
nmap <leader>b :Telescope buffers<CR>

lua << EOF
require('telescope').load_extension('coc')
EOF
